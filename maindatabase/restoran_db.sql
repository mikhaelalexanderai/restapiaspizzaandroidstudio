-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2022 at 04:17 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restoran_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `nomor` int(11) NOT NULL,
  `no_transaksi` int(10) NOT NULL,
  `id_makanan` char(5) NOT NULL,
  `harga` mediumint(9) NOT NULL,
  `jumlah` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`nomor`, `no_transaksi`, `id_makanan`, `harga`, `jumlah`) VALUES
(14, 10, 'P0005', 65000, 1),
(15, 11, 'P0005', 65000, 1),
(16, 12, 'P0005', 65000, 1),
(23, 14, 'P0005', 65000, 1),
(24, 15, 'P0005', 65000, 1),
(25, 16, 'P0001', 60000, 1),
(26, 17, 'P0005', 65000, 1),
(27, 18, 'P0005', 65000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id_makanan` char(5) NOT NULL,
  `nama_makanan` varchar(50) NOT NULL,
  `harga` mediumint(9) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id_makanan`, `nama_makanan`, `harga`, `gambar`, `status`) VALUES
('P0001', 'American Beef', 60000, 'http://192.168.18.116/rest_api/foto/American Beef.png', 'Aktif'),
('P0005', 'Chicken Spicy Mozzarella', 65000, 'http://192.168.18.116/rest_api/foto/Chicken Spicy Mozzarella.png', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `username` varchar(35) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `level` enum('Kasir','Pelayan','Owner','Admin') NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `nama`, `level`, `password`) VALUES
('coba@gmail.com', 'coba lagi', 'Kasir', 'coba123'),
('cobalagi@gmail.com', 'coba lagi', 'Kasir', 'coba123'),
('kasir@kasir.com', 'Kasir2', 'Kasir', 'password'),
('stevi.ema@amikom.ac.id', 'Stevi Ema Wijayanti', 'Owner', '12345'),
('steviema.wijayanti@gmail.com', 'Stevi', 'Admin', 'qwerty');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `no_transaksi` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `username` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`no_transaksi`, `tanggal`, `username`) VALUES
(8, '2022-01-24', 'kasir@kasir.com'),
(9, '2022-01-24', 'kasir@kasir.com'),
(10, '2022-01-24', 'kasir@kasir.com'),
(11, '2022-01-24', 'kasir@kasir.com'),
(12, '2022-01-24', 'kasir@kasir.com'),
(13, '2022-01-24', 'kasir@kasir.com'),
(14, '2022-01-24', 'kasir@kasir.com'),
(15, '2022-01-24', 'kasir@kasir.com'),
(16, '2022-01-24', 'kasir@kasir.com'),
(17, '2022-01-24', 'kasir@kasir.com'),
(18, '2022-01-24', 'kasir@kasir.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`nomor`),
  ADD KEY `no_transaksi` (`no_transaksi`,`id_makanan`),
  ADD KEY `id_makanan` (`id_makanan`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id_makanan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `no_transaksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `detail_penjualan_ibfk_2` FOREIGN KEY (`id_makanan`) REFERENCES `makanan` (`id_makanan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_penjualan_ibfk_3` FOREIGN KEY (`no_transaksi`) REFERENCES `penjualan` (`no_transaksi`);

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `penjualan_ibfk_1` FOREIGN KEY (`username`) REFERENCES `pengguna` (`username`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
