<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Makanan_model extends CI_Model
{


    function __construct()
    {
        parent::__construct();
      
        $this->load->database();
    }

    
    public function getMakanan($nama)
    {
        if ($nama == '') {
         
            $data = $this->db->get('makanan');
        } else {
            
            $this->db->like('nama_makanan', $nama);
            $data = $this->db->get('makanan');
        }

        return $data->result_array();
    }

   
    public function insertMakanan($data)
    {
      
        $this->db->where('id_makanan', $data['id_makanan']);
        $check_data = $this->db->get('makanan');
        $result = $check_data->result_array();

        if (empty($result)) {
            
            $this->db->insert('makanan', $data);
        } else {
            $data = array();
        }

        return $data;
    }

    public function updateMakanan($data, $id_makanan)
    {
        
        $this->db->where('id_makanan', $id_makanan);
        $this->db->update('makanan', $data);

        $result = $this->db->get_where('makanan', array('id_makanan' => $id_makanan));

        return $result->row_array();
    }

    public function get_by_id($id)
    {
        return $this->db->get_where('makanan', $id);
    }

    public function deleteMakanan($id_makanan)
    {
        $result = $this->db->get_where('makanan', array('id_makanan' => $id_makanan));
        //delete data
        $this->db->where('id_makanan', $id_makanan);
        $this->db->delete('makanan');

        return $result->row_array();
    }
}
