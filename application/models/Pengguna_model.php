<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_model extends CI_Model{

    function __construct(){
        parent::__construct();
       
        $this->load->database();
    }

    public function getPengguna($nama){
        if($nama==''){
            
            $data = $this->db->get('pengguna');
        }else{
            
            $this->db->like('nama', $nama);
            $this->db->or_like('username', $nama);
            $data = $this->db->get('pengguna');
        }

        return $data->result_array();
    }

 
    public function insertPengguna($data){
       
        $this->db->where('username', $data['username']);
        $check_data = $this->db->get('pengguna');
        $result = $check_data->result_array();

        if(empty($result)){
            
            $this->db->insert('pengguna', $data);
        }else{
            $data = array();
        }

        return $data;
    }


    public function updatePengguna($data, $username){
        
        $this->db->where('username', $username);
        $this->db->update('pengguna', $data);

        $result = $this->db->get_where('pengguna', array('username' => $username));

        return $result->row_array();
    }


    public function deletePengguna($username){
        $result = $this->db->get_where('pengguna', array('username' => $username));
      
        $this->db->where('username', $username);
        $this->db->delete('pengguna');

        return $result->row_array();
    }

 
    public function login($username, $password){
        
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $data = $this->db->get('pengguna');

        return $data->row_array();
    }
}
