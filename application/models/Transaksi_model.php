<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model{

   
    function __construct(){
        parent::__construct();
     
        $this->load->database();
    }


    public function laporanharian()
    {
       
        $dataHarian = $this->db->query('SELECT penjualan.no_transaksi, tanggal, SUM(harga) AS total FROM penjualan JOIN detail_penjualan ON penjualan.no_transaksi = detail_penjualan.no_transaksi WHERE tanggal = CURDATE() GROUP BY penjualan.no_transaksi, tanggal');
        return $dataHarian->result_array();
    }

  
    public function insertPenjualan($data){
        $this->db->insert('penjualan', $data);

        $last_id = $this->db->insert_id();
        $data['last_id'] = $last_id;

        return $data;
    }

    
    public function insertDetailPenjualan($data){
        $this->db->insert('detail_penjualan', $data);

        $last_id = $this->db->insert_id();
        $data['last_id'] = $last_id;

        return $data;
    }
}
?>
