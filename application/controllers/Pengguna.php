<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Pengguna extends REST_Controller{

   
    function __construct(){
        parent::__construct();
        $this->load->model('Pengguna_model');
    }

    
    function index_get(){
        

        
        $nama = $this->get('nama');
      
        $data = $this->Pengguna_model->getPengguna($nama);

        
        $result = $data;

        $this->response($result, REST_Controller::HTTP_OK);
    }

   
    function index_post(){
        

        
        $validasi_message = [];

        
        if($this->post('username') == ''){
            array_push($validasi_message,'Email (username) can not be empty');
        }

        
        if($this->post('username') != '' && !filter_var($this->post('username'), FILTER_VALIDATE_EMAIL)){
            array_push($validasi_message,'Email is invalid');
        }

       
        if($this->post('nama') == '' ){
            array_push($validasi_message,'Name can not be blank');
        }

        
        if($this->post('level') == '' ){
            array_push($validasi_message,'Level can not be empty');
        }

      
        if($this->post('password') == '' ){
            array_push($validasi_message,'Password can not be empty');
        }

       
        if(count($validasi_message) > 0){
            $output = array(
                'success' => false,
                'message' => 'insert data failed, data not valid',
                'data' => $validasi_message
            );

            $this->response($output, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }

        $data = array(
            'username' => $this->post('username'),
            'nama' => $this->post('nama'),
            'level' => $this->post('level'),
            'password' => $this->post('password')
        );

     
        $result = $this->Pengguna_model->insertPengguna($data);

     
        if(empty($result)){
            $output = array(
                'success' => false,
                'message' => 'data already exists',
                'data' => null
            );
        }else{
            $output = array(
                'success' => true,
                'message' => 'insert data success',
                'data' => array(
                    'pengguna' => $result
                )
            );
        }
        $this->response($output, REST_Controller::HTTP_OK);
    }

  
    function index_put(){
        
      
        $username = $this->put('username');

    
        $validasi_message = [];

    
        if($username == ''){
            array_push($validasi_message,'Email (username) can not be empty');
        }

       
        if($username != '' && !filter_var($username, FILTER_VALIDATE_EMAIL)){
            array_push($validasi_message,'Email is invalid');
        }

    
        if($this->put('nama') == '' ){
            array_push($validasi_message,'Name can not be blank');
        }

  
        if($this->put('level') == '' ){
            array_push($validasi_message,'Level can not be empty');
        }

    
        if($this->put('password') == '' ){
            array_push($validasi_message,'Password can not be empty');
        }

        
        if(count($validasi_message) > 0){
            $output = array(
                'success' => false,
                'message' => 'update data failed, data not valid',
                'data' => $validasi_message
            );

            $this->response($output, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }

        
        $data = array(
            'nama' => $this->put('nama'),
            'level' => $this->put('level'),
            'password' => $this->put('password')
        );

        
        $result = $this->Pengguna_model->updatePengguna($data, $username);

        //response
        $output = array(
            'success' => true,
            'message' => 'update data success',
            'data' => array(
                'pengguna' => $result
            )
        );

        $this->response($output, REST_Controller::HTTP_OK);
    }

    
    function index_delete(){
       

        
        $username = $this->delete('username');

      
        $validasi_message = [];

        
        if($username == ''){
            array_push($validasi_message,'Email (username) can not be empty');
        }

      
        if($username != '' && !filter_var($username, FILTER_VALIDATE_EMAIL)){
            array_push($validasi_message,'Email is invalid');
        }

    
        if(count($validasi_message) > 0){
            $output = array(
                'success' => false,
                'message' => 'delete data failed, email/username is invalid',
                'data' => $validasi_message
            );

            $this->response($output, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }

      
        $result = $this->Pengguna_model->deletePengguna($username);

      
        if(empty($result)){
            $output = array(
                'success' => false,
                'message' => 'email/username not found',
                'data' => null
            );
        }else{
            $output = array(
                'success' => true,
                'message' => 'delete data success',
                'data' => array(
                    'pengguna' => $result
                )
            );
        }

        $this->response($output, REST_Controller::HTTP_OK);
    }

   
    function login_post(){
        
        $username = $this->post('username');
        $password = $this->post('password');

      
        $data = $this->Pengguna_model->login($username, $password);

        if(empty($data)){
            $output = array(
                'success' => false,
                'message' => 'username/password is invalid',
                'data' => null,
                'error_code' => 1308
            );
            $this->response($output, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }else{
            
            $jwt = new JWT();

            
            $secret_key = 'JKq5GpLkbCr39JlAqTRc56JHsblOjan90PsnamS312KSlkfd';

            $date = new Datetime();
        
            $payload = array(
                'username' => $data['username'],
                'name' => $data['nama'],
                'user type' => $data['level'],
                'login_time' => $date->getTimeStamp(),
                'expired_time' => $date->getTimeStamp() + 1800
            );
   
            $result = $data;
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    
    function token_check(){
        try{
            $token = $this->input->get_request_header('Authorization');

            if(!empty($token)){
                $token = explode(' ', $token)[1];
            }

           
            $jwt = new JWT();

            
            $secret_key = 'JKq5GpLkbCr39JlAqTRc56JHsblOjan90PsnamS312KSlkfd';

            $token_decode = $jwt->decode($token, $secret_key);
        }catch(Exception $e){
            $result = array(
                'success' => false,
                'message' => 'token is invalid',
                'data' => null,
                'error_code' => 1204
            );
            $this->response($result, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }
        
    }
}
