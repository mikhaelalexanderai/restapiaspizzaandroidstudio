<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Login extends REST_Controller{
  
    function __construct(){
        parent::__construct();
        $this->load->model('Pengguna_model');
    }

    function index_post(){
       
        $username = $this->post('username');
        $password = $this->post('password');

       
        $data = $this->Pengguna_model->login($username, $password);

     
        if(empty($data)){
            $output = array(
                'success' => false,
                'message' => 'username/password is invalid',
                'data' => null,
                'error_code' => 1308
            );
            $this->response($output, REST_Controller::HTTP_OK);
            $this->output->_display();
            exit();
        }else{
            
            $jwt = new JWT();

           
            $secret_key = 'JKq5GpLkbCr39JlAqTRc56JHsblOjan90PsnamS312KSlkfd';

            $date = new Datetime();
            
            $payload = array(
                'username' => $data['username'],
                'name' => $data['nama'],
                'user type' => $data['level'],
                'login_time' => $date->getTimeStamp(),
                'expired_time' => $date->getTimeStamp() + 1800
            );

            
            $result = array(
                'success' => true,
                'message' => 'login success',
                'data' => $data,
                'token' => $jwt->encode($payload, $secret_key)
            );
           
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }
}
